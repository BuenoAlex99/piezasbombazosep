
create database almacenesBueno;
use almacenesBueno;

create table usuario(
idUsuario int primary key auto_increment,
nombreUsuario varchar(50) unique,
contrasena varchar(100),
tipoUsuario varchar(30)
);

create table cliente(
 idCliente int primary key auto_increment,
nombreCliente varchar(45),
direccionCliente varchar(50),
telefonoCliente int,
correoCliente varchar(60),
notasCliente  varchar(500) 
);

create table proveedor(
 idProveedor int primary key auto_increment,
 nombreProveedor varchar(45), 
 direccionProveedor varchar(50), 
 telefonoProveedor int,
 correoProveedor varchar(60),
 notasProveedor varchar(500) 
);

create table producto(
idProducto int primary key auto_increment,
idProveedor int,
nombre varchar(100),
peso double,
tamano double,
stock int,
precioCompraProducto double,
precioVentaProducto double,
constraint fk_pedidoCompraProveedor foreign key (idProveedor) references proveedor(idProveedor)
);

create table pedidoCompra(
idPedidoCompra int primary key auto_increment,
producto varchar(100),
cantidad int,
fechaPedido date,
precioCompra double,
idProveedor int,
indicadorRecibido varchar(50),
fechaRecibido date, 
indicadorCobro varchar(50),
constraint fk_pedidoCompraProveedor foreign key (idProveedor) references proveedor(idProveedor)
);

create table pedidoVenta(
idPedidoVenta int primary key auto_increment,
producto varchar(100),
cantidad int,
fechaPedido date,
precioVenta double,
idCliente int,
indicadorEnvio varchar(50),
fechaEnvio date,
indicadorCobro varchar(50),
fechaCobro date,
constraint fk_pedidoVentaCliente foreign key (idCliente) references cliente(idCliente)
);