package MVC;

import Enumeradas.TipoUsuario;
import Vistas.Administrador;
import Vistas.Cliente;
import Vistas.Login;
import Vistas.Proveedor;
import Soporte.Correo;
import net.sf.jasperreports.data.cache.NumberToDateTransformer;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.export.JRPdfExporter;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.AbstractDocument;
import java.awt.event.*;
import java.sql.*;
import java.time.LocalDate;
import java.util.Date;
import java.util.Vector;

public class Controlador implements ActionListener, ItemListener, ListSelectionListener, WindowListener {

    private Modelo modelo;
    private Administrador administrador;
    private Cliente cliente;
    private Proveedor proveedor;
    private Login login;
    private Correo correo;

    public Controlador(Modelo modelo, Administrador administrador, Cliente cliente,
                       Proveedor proveedor, Login login, Correo correo) throws SQLException {
        this.modelo = modelo;
        this.administrador = administrador;
        this.cliente = cliente;
        this.proveedor = proveedor;
        this.login = login;
        this.correo = correo;
        modelo.conectar();
        addActionListeners(this);

        addWindowListeners(this);
        addActionListenerTable();
        refrescarTodo();
    }

    public void refrescarTodo() {
        refrescarClienteAdministrador();
        refrescarProveedorAdministracion();
        refrescarCompra();
        refrescarVenta();
        refrescarCompraBuscar();
        refrescarVentaBuscar();
        refrescarUsuarioAdministrador();
        refrescarProveedorDepCompras();
        refrescarUsuarioDepCompras();
        refrescarClienteDepVentas();
        refrescarUsuarioDepVentas();
    }

    private void addActionListeners(ActionListener listener){
        login.ACCEDERButton.addActionListener(listener);
        //ADMINISTRACION

        administrador.altaCliente.addActionListener(listener);
        administrador.modificarCliente.addActionListener(listener);
        administrador.eliminarCliente.addActionListener(listener);

        administrador.altaProveedor.addActionListener(listener);
        administrador.modificarProveedor.addActionListener(listener);
        administrador.eliminarProveedor.addActionListener(listener);

        administrador.ALTAcompra.addActionListener(listener);
        administrador.ELIMINARcompra.addActionListener(listener);
        administrador.PDFcompra.addActionListener(listener);
        administrador.BUSCARcompra.addActionListener(listener);

        administrador.ALTAventa.addActionListener(listener);
        administrador.ELIMINARventa.addActionListener(listener);
        administrador.PDFventa.addActionListener(listener);
        administrador.BUSCARventa.addActionListener(listener);

        administrador.enviarButton.addActionListener(listener);

        administrador.altaUsuario.addActionListener(listener);
        administrador.eliminarUsuario.addActionListener(listener);
        administrador.modificarUsuario.addActionListener(listener);
        administrador.buscarUsuarioButton.addActionListener(listener);
        administrador.INICIO.addActionListener(listener);

        //DEPARTAMENTO VENTAS

        cliente.altaClienteButton.addActionListener(listener);

        cliente.eliminarClienteButton.addActionListener(listener);
        cliente.eliminarUsuarioButton.addActionListener(listener);

        cliente.modificarClienteButton.addActionListener(listener);
        cliente.modificarUsuarioButton.addActionListener(listener);

        cliente.enviarButton.addActionListener(listener);

        //DEPARTAMENTO COMPRAS
        proveedor.altaProveedorButton.addActionListener(listener);

        proveedor.eliminarProveedorButton.addActionListener(listener);
        proveedor.eliminarUsuarioButton.addActionListener(listener);

        proveedor.modificarProveedorButton.addActionListener(listener);
        proveedor.modificarUsuarioButton.addActionListener(listener);

        proveedor.enviarButton.addActionListener(listener);

    }

    private void addWindowListeners(WindowListener listener){
        administrador.addWindowListener(listener);
        cliente.addWindowListener(listener);
        proveedor.addWindowListener(listener);
        login.addWindowListener(listener);
    }

    private void addActionListenerTable() {
        administrador.tablaClientes.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                int row = administrador.tablaClientes.getSelectedRow();
                administrador.nombreClienteTxt.setText(String.valueOf(administrador.tablaClientes.getValueAt(row, 1)));
                administrador.direccionClienteTxt.setText(String.valueOf(administrador.tablaClientes.getValueAt(row, 2)));
                administrador.telefonoClienteTxt.setText(String.valueOf(administrador.tablaClientes.getValueAt(row, 3)));
                administrador.correoClienteTxt.setText(String.valueOf(administrador.tablaClientes.getValueAt(row, 4)));
                administrador.notasClienteTxt.setText(String.valueOf(administrador.tablaClientes.getValueAt(row, 5)));
            }
        });

        administrador.tablaProveedor.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                int row = administrador.tablaProveedor.getSelectedRow();
                administrador.nombreProveedorTxt.setText(String.valueOf(administrador.tablaProveedor.getValueAt(row, 1)));
                administrador.direccionProveedorTxt.setText(String.valueOf(administrador.tablaProveedor.getValueAt(row, 2)));
                administrador.telefonoProveedorTxt.setText(String.valueOf(administrador.tablaProveedor.getValueAt(row, 3)));
                administrador.correoProveedorTxt.setText(String.valueOf(administrador.tablaProveedor.getValueAt(row, 4)));
                administrador.notasProveedorTxt.setText(String.valueOf(administrador.tablaProveedor.getValueAt(row, 5)));
            }
        });

        administrador.tablaCompras.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                int row = administrador.tablaCompras.getSelectedRow();
                administrador.productoCompraCombo.setSelectedItem(String.valueOf(administrador.tablaCompras.getValueAt(row,1)));
                administrador.cantidadCompra.setText(String.valueOf(administrador.tablaCompras.getValueAt(row,2)));
                administrador.fechaPedidoDatePicker.setDate(LocalDate.parse(String.valueOf(administrador.tablaCompras.getValueAt(row,3))));
                administrador.precioCompra.setText(String.valueOf(administrador.tablaCompras.getValueAt(row,4)));
                administrador.proveedorCombo.setSelectedItem(String.valueOf(administrador.tablaCompras.getValueAt(row,5)));
                administrador.indicadorRecibidoCombo.setSelectedItem(String.valueOf(administrador.tablaCompras.getValueAt(row, 6)));
                administrador.fechaRecibidoDatePicker.setDate(LocalDate.parse(String.valueOf(administrador.tablaCompras.getValueAt(row, 7))));
                administrador.indicadorCobroCombo.setSelectedItem(String.valueOf(administrador.tablaCompras.getValueAt(row, 8)));
            }
        });

        administrador.tablaVenta.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                int row = administrador.tablaVenta.getSelectedRow();
                administrador.productoVentaCombo.setSelectedItem(String.valueOf(administrador.tablaVenta.getValueAt(row,1)));
                administrador.cantidadVenta.setText(String.valueOf(administrador.tablaVenta.getValueAt(row,2)));
                administrador.fechaPedidoVentaDatePicker.setDate(LocalDate.parse(String.valueOf(administrador.tablaVenta.getValueAt(row,3))));
                administrador.precioVenta.setText(String.valueOf(administrador.tablaVenta.getValueAt(row,4)));
                administrador.clienteCombo.setSelectedItem(String.valueOf(administrador.tablaVenta.getValueAt(row,5)));
                administrador.indicadorEnvioCombo.setSelectedItem(String.valueOf(administrador.tablaVenta.getValueAt(row, 6)));
                administrador.fechaEnvioVentaDatePicker.setDate(LocalDate.parse(String.valueOf(administrador.tablaVenta.getValueAt(row, 7))));
                administrador.indicadorCobroVenta.setSelectedItem(String.valueOf(administrador.tablaVenta.getValueAt(row, 8)));
                administrador.fechaCobroVentaDatePicker.setDate(LocalDate.parse(String.valueOf(administrador.tablaVenta.getValueAt(row, 9))));
            }
        });

        administrador.tablaUsuariosAdministracion.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                int row = administrador.tablaUsuariosAdministracion.getSelectedRow();
                administrador.usuarioAdministracionTxt.setText(String.valueOf(administrador.tablaUsuariosAdministracion.getValueAt(row, 1)));
                administrador.passwordUsuarioAdministracion.setText(String.valueOf(administrador.tablaUsuariosAdministracion.getValueAt(row, 2)));
                administrador.tipoUsuarioCombo.setSelectedItem(String.valueOf(administrador.tablaUsuariosAdministracion.getValueAt(row, 3)));
            }
        });

        cliente.tablaClientes.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                int row = cliente.tablaClientes.getSelectedRow();
                cliente.nombreCliente.setText(String.valueOf(cliente.tablaClientes.getValueAt(row, 1)));
                cliente.direccionCliente.setText(String.valueOf(cliente.tablaClientes.getValueAt(row, 2)));
                cliente.telefonoCliente.setText(String.valueOf(cliente.tablaClientes.getValueAt(row, 3)));
                cliente.correoCliente.setText(String.valueOf(cliente.tablaClientes.getValueAt(row, 4)));
                cliente.notasCliente.setText(String.valueOf(cliente.tablaClientes.getValueAt(row, 5)));
            }
        });

        cliente.tablaUsuariosCliente.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                int row = cliente.tablaUsuariosCliente.getSelectedRow();
                cliente.usuarioRegistroCliente.setText(String.valueOf(cliente.tablaUsuariosCliente.getValueAt(row, 1)));
                cliente.contrasenaRegistroCliente.setText(String.valueOf(cliente.tablaUsuariosCliente.getValueAt(row, 2)));
                cliente.tipoUsuarioCombo.setSelectedItem(String.valueOf(cliente.tablaUsuariosCliente.getValueAt(row, 3)));
            }
        });

        proveedor.tablaProveedor.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                int row = proveedor.tablaProveedor.getSelectedRow();
                proveedor.nombreProveedor.setText(String.valueOf(proveedor.tablaProveedor.getValueAt(row, 1)));
                proveedor.direccionProveedor.setText(String.valueOf(proveedor.tablaProveedor.getValueAt(row, 2)));
                proveedor.telefonoProveedor.setText(String.valueOf(proveedor.tablaProveedor.getValueAt(row, 3)));
                proveedor.correoProveedor.setText(String.valueOf(proveedor.tablaProveedor.getValueAt(row, 4)));
                proveedor.notasProveedor.setText(String.valueOf(proveedor.tablaProveedor.getValueAt(row, 5)));
            }
        });

        proveedor.tablaUsuariosRegistro.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                int row = proveedor.tablaUsuariosRegistro.getSelectedRow();
                proveedor.UsuarioRegistroProveedor.setText(String.valueOf(proveedor.tablaUsuariosRegistro.getValueAt(row, 1)));
                proveedor.ContrasenaRegistroProveedor.setText(String.valueOf(proveedor.tablaUsuariosRegistro.getValueAt(row, 2)));
                proveedor.tipoUsuarioCombo.setSelectedItem(String.valueOf(proveedor.tablaUsuariosRegistro.getValueAt(row, 3)));
            }
        });
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String command = e.getActionCommand();
        switch (command){
            case "INICIO":
                login.setVisible(true);
                administrador.setVisible(false);
                administrador.dispose();
                break;
            case "ENTRAR":
                if(comprobarRegistroLoginVacio()){
                    JOptionPane.showMessageDialog(null, "Error, datos introducidos incorrectos");
                } else {
                    boolean comprobarLogin = modelo.comprobarDatosUsuario(login.usuarioLogin.getText(), login.contrasenaLogin.getText(),
                            String.valueOf(login.tipoUsuarioCombo.getSelectedItem()));
                    if (comprobarLogin){
                        if (login.tipoUsuarioCombo.getSelectedItem().equals(TipoUsuario.ADMINISTRADOR.getValor())) {
                            administrador.dispose();
                            administrador.setVisible(true);
                            login.setVisible(false);
                        } else if (login.tipoUsuarioCombo.getSelectedItem().equals(TipoUsuario.PROVEEDOR.getValor())) {
                            proveedor.setVisible(true);
                            login.setVisible(false);
                        } else if (login.tipoUsuarioCombo.getSelectedItem().equals(TipoUsuario.CLIENTE.getValor())) {
                            cliente.setVisible(true);
                            login.setVisible(false);
                        }
                    }else{
                        JOptionPane.showMessageDialog(null,"ERROR. Datos de inicio de sesión incorrectos.");
                    }
                }
                break;

                // DEPARTAMENTO ADMINISTRACION

            case "ALTA":
                if (comprobarClienteVacio()){
                    JOptionPane.showMessageDialog(null, "Rellena todos los campos");
                    administrador.tablaClientes.clearSelection();
                } else{
                    modelo.insertarCliente(
                            administrador.nombreClienteTxt.getText(),
                            administrador.direccionClienteTxt.getText(),
                            Integer.parseInt(administrador.telefonoClienteTxt.getText()),
                            administrador.correoClienteTxt.getText(),
                            administrador.notasClienteTxt.getText()
                    );
                }
                borrarCamposClientes();
                refrescarClienteAdministrador();
                break;
            case "MODIFICAR":
                modelo.modificarCliente(
                        administrador.nombreClienteTxt.getText(),
                        administrador.direccionClienteTxt.getText(),
                        Integer.parseInt(administrador.telefonoClienteTxt.getText()),
                        administrador.correoClienteTxt.getText(),
                        administrador.notasClienteTxt.getText(),
                        Integer.parseInt(String.valueOf(administrador.tablaClientes.getValueAt(
                                administrador.tablaClientes.getSelectedRow(), 0
                        )))
                );
                borrarCamposClientes();
                refrescarClienteAdministrador();
                break;
            case "ELIMINAR":
                modelo.eliminarCliente((Integer) administrador.tablaClientes.getValueAt(
                        administrador.tablaClientes.getSelectedRow(), 0));
                borrarCamposClientes();
                refrescarClienteAdministrador();
                break;

            case "ALTAproveedor":
                if (comprobarProveedorVacio()){
                    JOptionPane.showMessageDialog(null, "Rellena todos los campos");
                    administrador.tablaProveedor.clearSelection();
                } else {
                    modelo.insertarProveedor(
                            administrador.nombreProveedorTxt.getText(),
                            administrador.direccionProveedorTxt.getText(),
                            Integer.parseInt(administrador.telefonoProveedorTxt.getText()),
                            administrador.correoProveedorTxt.getText(),
                            administrador.notasProveedorTxt.getText()
                    );
                }

                borrarCamposProveedor();
                refrescarProveedorAdministracion();
                break;
            case "MODIFICARproveedor":
                modelo.modificarProveedor(
                        administrador.nombreProveedorTxt.getText(),
                        administrador.direccionProveedorTxt.getText(),
                        Integer.parseInt(administrador.telefonoProveedorTxt.getText()),
                        administrador.correoProveedorTxt.getText(),
                        administrador.notasProveedorTxt.getText(),
                        Integer.parseInt(String.valueOf(administrador.tablaProveedor.getValueAt(
                                administrador.tablaProveedor.getSelectedRow(),0
                        )))
                );
                borrarCamposProveedor();
                refrescarProveedorAdministracion();
                break;
            case "ELIMINARproveedor":
                modelo.eliminarProveedor((Integer) administrador.tablaProveedor.getValueAt(
                        administrador.tablaProveedor.getSelectedRow(), 0));
                borrarCamposProveedor();
                refrescarProveedorAdministracion();
                break;
            case "enviarAdministrador":

                    String destinatarioAdministracion = administrador.destinatarioTxt.getText();
                    String asuntoAdministracion = administrador.asuntoCorreo.getText();
                    String cuerpoAdministracion = administrador.cuerpoCorreo.getText();
                    correo.enviarMail(destinatarioAdministracion, asuntoAdministracion, cuerpoAdministracion);

                break;
            case "ALTAcompra":
                    if (comprobarCompraVacia()){
                        JOptionPane.showMessageDialog(null, "Rellena todos los huecos");
                    } else {
                        modelo.altaCompra(
                                String.valueOf(administrador.productoCompraCombo.getSelectedItem()),
                                Integer.parseInt(administrador.cantidadCompra.getText()),
                                administrador.fechaPedidoDatePicker.getDate(),
                                Double.parseDouble(administrador.precioCompra.getText()),
                                String.valueOf(administrador.proveedorCombo.getSelectedItem()),
                                String.valueOf(administrador.indicadorRecibidoCombo.getSelectedItem()),
                                administrador.fechaRecibidoDatePicker.getDate(),
                                String.valueOf(administrador.indicadorCobroCombo.getSelectedItem())

                                );
                    }
                    borrarCamposCompraAdministrador();
                    refrescarCompra();
                break;
            case "ELIMINARcompra":
                modelo.eliminarCompra((Integer) administrador.tablaCompras.getValueAt(
                        administrador.tablaCompras.getSelectedRow(), 0));
                borrarCamposCompraAdministrador();
                refrescarCompra();
                break;
            case "PDFcompra":
                try {
                    Connection conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306/almacenesBueno", "root", "");

                    JasperReport reporte= JasperCompileManager.compileReport("Jasper\\Compras.jrxml");



                    JasperPrint jasperPrint = JasperFillManager.fillReport(reporte, null, conexion);

                    JRExporter exporter = new JRPdfExporter();

                    exporter.setParameter(JRExporterParameter.JASPER_PRINT,jasperPrint);
                    exporter.setParameter(JRExporterParameter.OUTPUT_FILE,new java.io.File("Jasper\\Compras.pdf"));

                    exporter.exportReport();
                } catch (JRException | SQLException ex) {
                    ex.printStackTrace();
                }
                break;
            case "BUSCARcompra":
                String buscarProducto = (String) administrador.busquedaCompraCombo.getSelectedItem();
                try {
                    ResultSet rs = modelo.consultarBusquedaCompra(buscarProducto);
                    refrescarCompraBuscar();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                break;
            case "ALTAventa":
                if (comprobarVentaVacio()){
                    JOptionPane.showMessageDialog(null, "Rellena todos los campos");
                } else {
                    modelo.altaVenta(
                            String.valueOf(administrador.productoVentaCombo.getSelectedItem()),
                            Integer.parseInt(administrador.cantidadVenta.getText()),
                            administrador.fechaPedidoVentaDatePicker.getDate(),
                            Double.parseDouble(administrador.precioVenta.getText()),
                            String.valueOf(administrador.clienteCombo.getSelectedItem()),
                            String.valueOf(administrador.indicadorEnvioCombo.getSelectedItem()),
                            administrador.fechaEnvioVentaDatePicker.getDate(),
                            String.valueOf(administrador.indicadorCobroVenta.getSelectedItem()),
                            administrador.fechaCobroVentaDatePicker.getDate()
                    );
                }
                borrarCamposVentaAdministracion();
                refrescarVenta();
                break;
            case "ELIMINARventa":
                modelo.eliminarVenta((Integer) administrador.tablaVenta.getValueAt(
                        administrador.tablaVenta.getSelectedRow(), 0));
                borrarCamposVentaAdministracion();
                refrescarVenta();
                break;
            case "PDFventa":
                try {
                    Connection conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306/almacenesBueno", "root", "");

                    JasperReport reporte= JasperCompileManager.compileReport("Jasper\\Ventas.jrxml");



                    JasperPrint jasperPrint = JasperFillManager.fillReport(reporte, null, conexion);

                    JRExporter exporter = new JRPdfExporter();

                    exporter.setParameter(JRExporterParameter.JASPER_PRINT,jasperPrint);
                    exporter.setParameter(JRExporterParameter.OUTPUT_FILE,new java.io.File("Jasper\\Ventas.pdf"));

                    exporter.exportReport();
                } catch (JRException | SQLException ex) {
                    ex.printStackTrace();
                }
                break;
            case "BUSCARventa":
                System.out.println("busca");
                String buscarVenta = (String) administrador.busquedaVentaCombo.getSelectedItem();
                try {
                    ResultSet rs = modelo.consultarBusquedaVenta(buscarVenta);
                    refrescarVentaBuscar();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                refrescarVentaBuscar();
                break;
            case "ALTAusuario":
                try {
                    if (comprobarRegistroAdministracionVacio()) {
                        JOptionPane.showMessageDialog(null, "Rellena todos los campos");
                    } else {

                        modelo.altaUsuario(
                                administrador.usuarioAdministracionTxt.getText(),
                                administrador.passwordUsuarioAdministracion.getText(),
                                String.valueOf(administrador.tipoUsuarioCombo.getSelectedItem())
                        );
                    }
                } catch (NumberFormatException nfe) {
                    JOptionPane.showMessageDialog(null, "Introduce información en los campos que lo requieren");
                }
                borrarCamposRegistroAdministracion();
                refrescarUsuarioAdministrador();
                break;
            case "ELIMINARusuario":
                modelo.eliminarUsuario((Integer) administrador.tablaUsuariosAdministracion.getValueAt(
                        administrador.tablaUsuariosAdministracion.getSelectedRow(), 0));
                borrarCamposRegistroAdministracion();
                refrescarUsuarioAdministrador();
                break;
            case "MODIFICARusuario":
                modelo.modificarUsuario(
                        administrador.usuarioAdministracionTxt.getText(),
                        administrador.passwordUsuarioAdministracion.getText(),
                        (String) administrador.tipoUsuarioCombo.getSelectedItem(),
                        Integer.parseInt(administrador.tablaUsuariosAdministracion.getValueAt(
                                administrador.tablaUsuariosAdministracion.getSelectedRow(), 0) + "")
                );
                refrescarUsuarioAdministrador();
                borrarCamposRegistroAdministracion();
                break;

                //DEPARTAMENTO VENTAS

            case "ALTAclienteDepVentas":
                if (comprobarClienteDepVentasVacio()){
                    JOptionPane.showMessageDialog(null, "Rellena todos los campos");
                    cliente.tablaClientes.clearSelection();
                } else{
                    modelo.insertarCliente(
                            cliente.nombreCliente.getText(),
                            cliente.direccionCliente.getText(),
                            Integer.parseInt(cliente.telefonoCliente.getText()),
                            cliente.correoCliente.getText(),
                            cliente.notasCliente.getText()
                    );
                }
                borrarCamposClienteDepVentas();
                refrescarClienteDepVentas();
                break;
            case "MODIFICARclienteDepVentas":
                modelo.modificarCliente(
                        cliente.nombreCliente.getText(),
                        cliente.direccionCliente.getText(),
                        Integer.parseInt(cliente.telefonoCliente.getText()),
                        cliente.correoCliente.getText(),
                        cliente.notasCliente.getText(),
                        Integer.parseInt(String.valueOf(cliente.tablaClientes.getValueAt(
                                cliente.tablaClientes.getSelectedRow(),0
                        )))
                );
                borrarCamposClienteDepVentas();
                refrescarClienteDepVentas();
                break;
            case "ELIMINARclienteDepVentas":
                modelo.eliminarCliente((Integer) cliente.tablaClientes.getValueAt(
                        cliente.tablaClientes.getSelectedRow(), 0
                ));
                borrarCamposClienteDepVentas();
                refrescarClienteDepVentas();
                break;
            case "ELIMINARusuarioDepVentas":
                modelo.eliminarUsuario((Integer) cliente.tablaUsuariosCliente.getValueAt(
                        cliente.tablaUsuariosCliente.getSelectedRow(), 0));
                borrarCamposRegistroDepVentas();
                refrescarUsuarioDepVentas();
                break;
            case "MODIFICARusuarioDepVentas":
                modelo.modificarUsuario(
                        cliente.usuarioRegistroCliente.getText(),
                        cliente.contrasenaRegistroCliente.getText(),
                        String.valueOf(cliente.tipoUsuarioCombo.getSelectedItem()),
                        Integer.parseInt(cliente.tablaUsuariosCliente.getValueAt(
                                cliente.tablaUsuariosCliente.getSelectedRow(), 0) + "")
                );
                refrescarUsuarioDepVentas();
                borrarCamposRegistroDepVentas();
                break;
            case "ENVIARcorreoDepVentas":
                if (comprobarCorreoDepVentasVacio()){
                    JOptionPane.showMessageDialog(null, "Rellena todos los campos");
                } else {
                   String destinatarioDepVentas =  cliente.clienteDestinatario.getText();
                   String asuntoDepVentas = cliente.cuerpoCorreo.getText();
                   String cuerpoDepVentas = cliente.cuerpoCorreo.getText();
                    correo.enviarMail(destinatarioDepVentas, asuntoDepVentas, cuerpoDepVentas);
                }

                break;

                // DEPARTAMENTO COMPRAS

            case "ALTAproveedorDepCompras":
                if (comprobarProveedorDepComprasVacio()){
                    JOptionPane.showMessageDialog(null, "Rellena todos los campos");
                    proveedor.tablaProveedor.clearSelection();
                } else {
                    modelo.insertarProveedor(
                            proveedor.nombreProveedor.getText(),
                            proveedor.direccionProveedor.getText(),
                            Integer.parseInt(proveedor.telefonoProveedor.getText()),
                            proveedor.correoProveedor.getText(),
                            proveedor.notasProveedor.getText()
                    );
                }

                borrarCamposProveedorDepCompras();
                refrescarProveedorDepCompras();
                break;
            case "MODIFICARproveedorDepCompras":
                modelo.modificarProveedor(
                        proveedor.nombreProveedor.getText(),
                        proveedor.direccionProveedor.getText(),
                        Integer.parseInt(proveedor.telefonoProveedor.getText()),
                        proveedor.correoProveedor.getText(),
                        proveedor.notasProveedor.getText(),
                        Integer.parseInt(String.valueOf(proveedor.tablaProveedor.getValueAt(
                                proveedor.tablaProveedor.getSelectedRow(),0
                        )))
                );
                borrarCamposProveedorDepCompras();
                refrescarProveedorDepCompras();
                break;
            case "ELIMINARproveedorDepCompras":
                modelo.eliminarProveedor((Integer) proveedor.tablaProveedor.getValueAt(
                        proveedor.tablaProveedor.getSelectedRow(),0
                        )
                );
                borrarCamposProveedorDepCompras();
                refrescarProveedorDepCompras();
                break;
            case "ELIMINARusuarioDepCompras":
                modelo.eliminarUsuario((Integer) proveedor.tablaUsuariosRegistro.getValueAt(
                        proveedor.tablaUsuariosRegistro.getSelectedRow(), 0));
                refrescarUsuarioDepCompras();
                borrarCamposRegistroDepCompras();

                break;
            case "MODIFICARusuarioDepCompras":
                System.out.println("modifica");
                modelo.modificarUsuario(
                        proveedor.UsuarioRegistroProveedor.getText(),
                        proveedor.ContrasenaRegistroProveedor.getText(),
                        String.valueOf(proveedor.tipoUsuarioCombo.getSelectedItem()),
                        Integer.parseInt(proveedor.tablaUsuariosRegistro.getValueAt(
                                proveedor.tablaUsuariosRegistro.getSelectedRow(), 0) + "")
                );
                refrescarUsuarioDepCompras();
                borrarCamposRegistroDepCompras();
                break;
            case "ENVIARcorreoDepCompras":
                if (comprobarCorreoDepComprasVacio()){
                    JOptionPane.showMessageDialog(null, "Rellena todos los campos");
                } else {
                    String  destinatarioDepCompras =  proveedor.proveedorDestinatario.getText();
                   String asuntoDepCompras = proveedor.asuntoCorreo.getText();
                   String cuerpoDepCompras = proveedor.cuerpoCorreo.getText();
                    correo.enviarMail(destinatarioDepCompras, asuntoDepCompras, cuerpoDepCompras);
                }

        }
    }

    private void refrescarVentaBuscar() {
        try {
            administrador.tablaBusquedaVenta.setModel(construirTableModelVentaBuscar(
                    modelo.consultarBusquedaVenta((String) administrador.busquedaVentaCombo.getSelectedItem())));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public DefaultTableModel construirTableModelVentaBuscar(ResultSet rs) throws SQLException{
        ResultSetMetaData metaData = rs.getMetaData();

        // names of columns
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }

        // data of the table
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);

        administrador.dtmTablaBusquedaVentas.setDataVector(data, columnNames);

        return administrador.dtmTablaBusquedaVentas;
    }

    public void refrescarCompraBuscar(){
        try {
            administrador.tablaBusquedaCompra.setModel(construirTableModelProductoBuscar(
                    modelo.consultarBusquedaCompra((String) administrador.busquedaCompraCombo.getSelectedItem())));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public DefaultTableModel construirTableModelProductoBuscar(ResultSet rs) throws SQLException{
        ResultSetMetaData metaData = rs.getMetaData();

        // names of columns
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }

        // data of the table
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);

        administrador.dtmTablaBusquedaCompras.setDataVector(data, columnNames);

        return administrador.dtmTablaBusquedaCompras;
    }

    public void refrescarVenta(){
        try {
            administrador.tablaVenta.setModel(construirTableModelVenta(modelo.consultarVenta()));
        } catch (SQLException sqle){
            sqle.printStackTrace();
        }
    }

    public DefaultTableModel construirTableModelVenta(ResultSet rs) throws SQLException{
        ResultSetMetaData metaData = rs.getMetaData();
        // names of columns
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }

        // data of the table
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);

        administrador.dtmTablaVentas.setDataVector(data, columnNames);

        return administrador.dtmTablaVentas;
    }

    public void refrescarCompra(){
        try {
            administrador.tablaCompras.setModel(construirTableModelCompras(modelo.consultarCompra()));
        } catch (SQLException sqle){
            sqle.printStackTrace();
        }
    }

    public DefaultTableModel construirTableModelCompras(ResultSet rs) throws SQLException{
        ResultSetMetaData metaData = rs.getMetaData();
        // names of columns
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }

        // data of the table
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);

        administrador.dtmTablaCompras.setDataVector(data, columnNames);

        return administrador.dtmTablaCompras;

    }

    public void refrescarClienteAdministrador(){
        try {
            administrador.tablaClientes.setModel(construirTableModelClientes(modelo.consultarCliente()));
            administrador.clienteCombo.removeAllItems();
            for(int i = 0; i < administrador.dtmTablaClientes.getRowCount(); i++){
                administrador.clienteCombo.addItem(
                        administrador.dtmTablaClientes.getValueAt(i,0)
                );
            }
        } catch (SQLException e){
            e.printStackTrace();
        }
    }

    public DefaultTableModel construirTableModelClientes(ResultSet rs) throws SQLException{
        ResultSetMetaData metaData = rs.getMetaData();

        // names of columns
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }

        // data of the table
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);

        administrador.dtmTablaClientes.setDataVector(data, columnNames);

        return administrador.dtmTablaClientes;

    }

    public void refrescarProveedorAdministracion(){
        try {
            administrador.tablaProveedor.setModel(construirTableModelProveedor(modelo.consultarProveedor()));
            for(int i = 0; i < administrador.dtmTablaProveedor.getRowCount(); i++){
                administrador.proveedorCombo.addItem(
                        administrador.dtmTablaProveedor.getValueAt(i,0)
                );
            }
        } catch (SQLException e){
            e.printStackTrace();
        }
    }

    public DefaultTableModel construirTableModelProveedor(ResultSet rs) throws SQLException{
        ResultSetMetaData metaData = rs.getMetaData();

        // names of columns
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }

        // data of the table
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);

        administrador.dtmTablaProveedor.setDataVector(data, columnNames);

        return administrador.dtmTablaProveedor;
    }

    public void refrescarClienteDepVentas(){
        try {
            cliente.tablaClientes.setModel(construirTableModelCliente(modelo.consultarCliente()));

        } catch (SQLException e){
            e.printStackTrace();
        }
    }

    public DefaultTableModel construirTableModelCliente(ResultSet rs) throws SQLException{
        ResultSetMetaData metaData = rs.getMetaData();

        // names of columns
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }

        // data of the table
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);

        cliente.dtmCliente.setDataVector(data, columnNames);

        return cliente.dtmCliente;

    }

    public void refrescarProveedorDepCompras(){
        try {
            proveedor.tablaProveedor.setModel(construirTableModelProveedorProveedor(modelo.consultarProveedor()));
        } catch (SQLException e){
            e.printStackTrace();
        }
    }

    public DefaultTableModel construirTableModelProveedorProveedor(ResultSet rs) throws SQLException{
        ResultSetMetaData metaData = rs.getMetaData();

        // names of columns
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }

        // data of the table
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);

        proveedor.dtmProveedor.setDataVector(data, columnNames);

        return proveedor.dtmProveedor;

    }

    public void refrescarUsuarioAdministrador(){
        try {
            administrador.tablaUsuariosAdministracion.setModel(construirTableModelUsuario(modelo.consultarRegistroAdministrador()));
        } catch (SQLException e){
            e.printStackTrace();
        }
    }

    public DefaultTableModel construirTableModelUsuario(ResultSet rs) throws SQLException{
        ResultSetMetaData metaData = rs.getMetaData();

        // names of columns
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }

        // data of the table
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);

        administrador.dtmTablaUsuarios.setDataVector(data, columnNames);

        return administrador.dtmTablaUsuarios;
    }

    public void refrescarUsuarioDepVentas(){
        try {
            cliente.tablaUsuariosCliente.setModel(construirTableModelUsuarioCliente(modelo.consultarRegistroCliente()));
        } catch (SQLException e){
            e.printStackTrace();
        }
    }

    public DefaultTableModel construirTableModelUsuarioCliente(ResultSet rs) throws SQLException{
        ResultSetMetaData metaData = rs.getMetaData();

        // names of columns
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }

        // data of the table
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);

        cliente.dtmRegistroCliente.setDataVector(data, columnNames);

        return cliente.dtmRegistroCliente;
    }

    public void refrescarUsuarioDepCompras(){
        try {
            proveedor.tablaUsuariosRegistro.setModel(construirTableModelUsuarioProveedor(modelo.consultarRegistroProveedor()));
        } catch (SQLException e){
            e.printStackTrace();
        }
    }

    public DefaultTableModel construirTableModelUsuarioProveedor(ResultSet rs) throws SQLException{
        ResultSetMetaData metaData = rs.getMetaData();

        // names of columns
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }

        // data of the table
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);

        proveedor.dtmRegistroProveedor.setDataVector(data, columnNames);

        return proveedor.dtmRegistroProveedor;
    }



    public void setDataVector(ResultSet rs, int columnCount, Vector<Vector<Object>> data) throws SQLException {
        while (rs.next()) {
            Vector<Object> vector = new Vector<>();
            for (int columnIndex = 1; columnIndex <= columnCount; columnIndex++) {
                vector.add(rs.getObject(columnIndex));
            }
            data.add(vector);
        }
    }

    public void borrarCamposClientes(){
        administrador.nombreClienteTxt.setText("");
        administrador.direccionClienteTxt.setText("");
        administrador.telefonoClienteTxt.setText("");
        administrador.correoClienteTxt.setText("");
        administrador.notasClienteTxt.setText("");
    }

    public void borrarCamposClienteDepVentas(){
        cliente.nombreCliente.setText("");
        cliente.direccionCliente.setText("");
        cliente.telefonoCliente.setText("");
        cliente.correoCliente.setText("");
        cliente.notasCliente.setText("");
    }

    public void borrarCamposProveedor(){
        administrador.nombreProveedorTxt.setText("");
        administrador.direccionProveedorTxt.setText("");
        administrador.telefonoProveedorTxt.setText("");
        administrador.correoProveedorTxt.setText("");
        administrador.notasProveedorTxt.setText("");
    }

    public void borrarCamposProveedorDepCompras(){
        proveedor.nombreProveedor.setText("");
        proveedor.direccionProveedor.setText("");
        proveedor.telefonoProveedor.setText("");
        proveedor.correoProveedor.setText("");
        proveedor.notasProveedor.setText("");
    }

    public void borrarCamposVentaAdministracion(){
        administrador.productoVentaCombo.setSelectedItem(-1);
        administrador.cantidadVenta.setText("");
        administrador.fechaPedidoVentaDatePicker.setText("");
        administrador.precioVenta.setText("");
        administrador.clienteCombo.setSelectedItem(-1);
        administrador.indicadorEnvioCombo.setSelectedItem(-1);
        administrador.fechaEnvioVentaDatePicker.setText("");
        administrador.indicadorCobroVenta.setSelectedItem(-1);
        administrador.fechaCobroVentaDatePicker.setText("");
    }

    public void borrarCamposCompraAdministrador(){
        administrador.productoCompraCombo.setSelectedItem(-1);
        administrador.cantidadCompra.setText("");
        administrador.fechaPedidoDatePicker.setText("");
        administrador.precioCompra.setText("");
        administrador.proveedorCombo.setSelectedItem(-1);
        administrador.indicadorRecibidoCombo.setSelectedItem(-1);
        administrador.fechaRecibidoDatePicker.setText("");
        administrador.indicadorCobroCombo.setSelectedItem(-1);
    }


    public void borrarCamposRegistroAdministracion(){
        administrador.usuarioAdministracionTxt.setText("");
        administrador.passwordUsuarioAdministracion.setText("");
        administrador.tipoUsuarioCombo.setSelectedItem(-1);
    }

    public void borrarCamposRegistroDepVentas(){
        cliente.usuarioRegistroCliente.setText("");
        cliente.contrasenaRegistroCliente.setText("");
        cliente.tipoUsuarioCombo.setSelectedItem(-1);
    }

    public void borrarCamposRegistroDepCompras(){
        proveedor.UsuarioRegistroProveedor.setText("");
        proveedor.ContrasenaRegistroProveedor.setText("");
        proveedor.tipoUsuarioCombo.setSelectedItem(-1);
    }

    public boolean comprobarClienteVacio(){
        return administrador.nombreClienteTxt.getText().isEmpty() ||
                administrador.direccionClienteTxt.getText().isEmpty() ||
                administrador.telefonoClienteTxt.getText().isEmpty() ||
                administrador.correoClienteTxt.getText().isEmpty() ||
                administrador.notasClienteTxt.getText().isEmpty();
    }

    public boolean comprobarProveedorVacio(){
        return administrador.nombreProveedorTxt.getText().isEmpty() ||
                administrador.direccionProveedorTxt.getText().isEmpty() ||
                administrador.telefonoProveedorTxt.getText().isEmpty() ||
                administrador.correoProveedorTxt.getText().isEmpty() ||
                administrador.notasProveedorTxt.getText().isEmpty();
    }

    public boolean comprobarCompraVacia(){
        return administrador.productoCompraCombo.getSelectedIndex() == -1||
                administrador.cantidadCompra.getText().isEmpty() ||
                administrador.fechaPedidoDatePicker.getText().isEmpty() ||
                administrador.precioCompra.getText().isEmpty() ||
                administrador.proveedorCombo.getSelectedIndex() == -1 ||
                administrador.indicadorRecibidoCombo.getSelectedIndex() == -1 ||
                administrador.fechaRecibidoDatePicker.getText().isEmpty() ||
                administrador.indicadorCobroCombo.getSelectedIndex() == -1;
    }

    public boolean comprobarVentaVacio(){
        return administrador.productoVentaCombo.getSelectedIndex() == -1 ||
                administrador.cantidadVenta.getText().isEmpty() ||
                administrador.fechaPedidoVentaDatePicker.getText().isEmpty() ||
                administrador.precioVenta.getText().isEmpty() ||
                administrador.clienteCombo.getSelectedIndex() == -1 ||
                administrador.indicadorEnvioCombo.getSelectedIndex() == -1 ||
                administrador.fechaEnvioVentaDatePicker.getText().isEmpty() ||
                administrador.indicadorCobroVenta.getSelectedIndex() == -1 ||
                administrador.fechaCobroVentaDatePicker.getText().isEmpty();
    }

    public boolean comprobarCorreoVacio(){
        return administrador.destinatarioTxt.getText().isEmpty() ||
                administrador.asuntoCorreo.getText().isEmpty() ||
                administrador.cuerpoCorreo.getText().isEmpty();
    }

    public boolean comprobarRegistroAdministracionVacio(){
        return administrador.usuarioAdministracionTxt.getText().isEmpty() ||
                administrador.passwordUsuarioAdministracion.getText().isEmpty() ||
                administrador.tipoUsuarioCombo.getSelectedIndex() == -1;
    }

    public boolean comprobarProveedorDepComprasVacio(){
        return proveedor.nombreProveedor.getText().isEmpty() ||
                proveedor.direccionProveedor.getText().isEmpty() ||
                proveedor.telefonoProveedor.getText().isEmpty() ||
                proveedor.correoProveedor.getText().isEmpty() ||
                proveedor.notasProveedor.getText().isEmpty();
    }

    public boolean comprobarCorreoDepComprasVacio(){
        return proveedor.proveedorDestinatario.getText().isEmpty() ||
                proveedor.asuntoCorreo.getText().isEmpty() ||
                proveedor.cuerpoCorreo.getText().isEmpty();
    }

    public boolean comprobarClienteDepVentasVacio(){
        return cliente.nombreCliente.getText().isEmpty() ||
                cliente.direccionCliente.getText().isEmpty() ||
                cliente.telefonoCliente.getText().isEmpty() ||
                cliente.correoCliente.getText().isEmpty() ||
                cliente.notasCliente.getText().isEmpty();
    }

    public boolean comprobarCorreoDepVentasVacio(){
        return cliente.clienteDestinatario.getText().isEmpty() ||
                cliente.asuntoCorreo.getText().isEmpty() ||
                cliente.cuerpoCorreo.getText().isEmpty();
    }

    public boolean comprobarRegistroLoginVacio(){
        return login.usuarioLogin.getText().isEmpty() ||
                login.contrasenaLogin.getText().isEmpty() ||
                login.tipoUsuarioCombo.getSelectedIndex() == -1;
    }



    @Override
    public void itemStateChanged(ItemEvent e) {

    }

    @Override
    public void windowOpened(WindowEvent e) {

    }

    @Override
    public void windowClosing(WindowEvent e) {

    }

    @Override
    public void windowClosed(WindowEvent e) {

    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }

    @Override
    public void valueChanged(ListSelectionEvent e) {

    }
}
