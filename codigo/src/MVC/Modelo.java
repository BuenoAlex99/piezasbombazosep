package MVC;

import javax.print.DocFlavor;
import java.sql.*;
import java.time.LocalDate;

public class Modelo {
    private static Connection conexion;

    /**
     * metodo cuya funcionalidad nos permite realizar una conexion desde nuestra aplicacion con nuestra base de datos
     */
    public void conectar() throws SQLException{
        conexion = null;
        conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306/almacenesBueno", "root", "");
    }

    //funcionalidad botones departamento administracion
    public void altaCompra(String producto, int cantidad, LocalDate fechaPedido, Double precioCompra, String idProveedor,
                           String indicadorRecibido, LocalDate fechaRecibido, String indicadorCobro){
        String consulta = "INSERT INTO pedidoCompra (producto, cantidad, fechaPedido, precioCompra, idProveedor, indicadorRecibido," +
                "fechaRecibido, indicadorCobro) VALUES (?,?,?,?,?,?,?,?)";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(consulta);
            sentencia.setString(1, producto);
            sentencia.setInt(2, cantidad);
            sentencia.setDate(3, Date.valueOf(fechaPedido));
            sentencia.setDouble(4, precioCompra);
            sentencia.setString(5, idProveedor);
            sentencia.setString(6, indicadorRecibido);
            sentencia.setDate(7, Date.valueOf(fechaRecibido));
            sentencia.setString(8, indicadorCobro);

            sentencia.executeUpdate();
        } catch (SQLException sqle){
            sqle.printStackTrace();
        }
    }



    public void eliminarCompra(int idPedidoCompra){
        String consulta = "DELETE FROM pedidoCompra WHERE idPedidoCompra = ?";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(consulta);
            sentencia.setInt(1, idPedidoCompra);
            sentencia.executeUpdate();
        } catch (SQLException sqle){
            sqle.printStackTrace();
        }
    }

    public void altaVenta(String idProducto, int cantidad, LocalDate fechaPedido, Double precioVenta, String idCliente,
                          String indicadorEnvio, LocalDate fechaEnvio, String indicadorCobro, LocalDate fechaCobro){
        String consulta = "INSERT INTO pedidoVenta (producto, cantidad, fechaPedido, precioVenta, idCliente, indicadorEnvio," +
                "fechaEnvio, indicadorCobro, fechaCobro) values (?,?,?,?,?,?,?,?,?)";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(consulta);
            sentencia.setString(1, idProducto);
            sentencia.setInt(2, cantidad);
            sentencia.setDate(3, Date.valueOf(fechaPedido));
            sentencia.setDouble(4, precioVenta);
            sentencia.setString(5, idCliente);
            sentencia.setString(6, indicadorEnvio);
            sentencia.setDate(7, Date.valueOf(fechaEnvio));
            sentencia.setString(8, indicadorCobro);
            sentencia.setDate(9, Date.valueOf(fechaCobro));

            sentencia.executeUpdate();
        } catch (SQLException sqle){
            sqle.printStackTrace();
        }
    }



    public void eliminarVenta(int idPedidoVenta){
        String consulta = "DELETE FROM pedidoVenta WHERE idPedidoVenta = ?";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(consulta);
            sentencia.setInt(1, idPedidoVenta);
            sentencia.executeUpdate();
        } catch (SQLException sqle){
            sqle.printStackTrace();
        }
    }

    //funcionalidad botones departamento compras(PROVEEDOR)

    /**
     *
     * @param nombre
     * @param direccion
     * @param telefono
     * @param correo
     * @param notas
     *
     * metodo el cual permite a nuestro departamento de compras poder insertar los distintos proveedores
     */
    public void insertarProveedor(String nombre, String direccion, int telefono, String correo, String notas){
        String consulta = "INSERT INTO proveedor (nombreProveedor, direccionProveedor, telefonoProveedor, correoProveedor" +
                ", notasProveedor) values(?,?,?,?,?)";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(consulta);
            sentencia.setString(1, nombre);
            sentencia.setString(2, direccion);
            sentencia.setInt(3, telefono);
            sentencia.setString(4, correo);
            sentencia.setString(5, notas);

            sentencia.executeUpdate();
        } catch (SQLException sqle){
            sqle.printStackTrace();
        }
    }

    /**
     *
     * @param idProveedor
     *
     * metodo el cual permite a nuestro departamento de ventas eliminar cualquier proveedor que deje
     * de realizar servicios con nosotros
     */
    public void eliminarProveedor(int idProveedor){
        String consulta = "DELETE FROM proveedor WHERE idProveedor = ?";

        PreparedStatement sentencia = null;
        try {
            sentencia = conexion.prepareStatement(consulta);
            sentencia.setInt(1, idProveedor);
            sentencia.executeUpdate();
        } catch (SQLException e){
            e.printStackTrace();
        }
    }

    /**
     *
     * @param nombre
     * @param direccion
     * @param telefono
     * @param correo
     * @param notas
     *
     * metodo el cual permite a nuestro departamento de ventas realizar cualquier modificacion en la ficha tecnica de
     * los proveedores
     */
    public void modificarProveedor(String nombre, String direccion, int telefono, String correo, String notas
    , int idProveedor){
        String consulta = "UPDATE proveedor SET nombreProveedor = ?, direccionProveedor = ?, telefonoProveedor = ?, " +
                "correoProveedor = ?, notasProveedor = ? "+
                " WHERE idProveedor = ?";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(consulta);
            sentencia.setString(1, nombre);
            sentencia.setString(2, direccion);
            sentencia.setInt(3, telefono);
            sentencia.setString(4, correo);
            sentencia.setString(5, notas);
            sentencia.setInt(6, idProveedor);

            sentencia.executeUpdate();
        } catch (SQLException e){
            e.printStackTrace();
        }
    }

    //funcionalidad botones departamento ventas(CLIENTE)

    /**
     *
     * @param nombre
     * @param direccion
     * @param telefono
     * @param correo
     * @param notas
     *
     * metodo el cual permite a nuestro departamento de ventas poder insertar los distintos clientes
     */
    public void insertarCliente(String nombre, String direccion, int telefono, String correo, String notas){
        String consulta = "INSERT INTO cliente (nombreCliente, direccionCliente, telefonoCliente, correoCliente, notasCliente) values(?,?,?,?,?)";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(consulta);
            sentencia.setString(1, nombre);
            sentencia.setString(2, direccion);
            sentencia.setInt(3, telefono);
            sentencia.setString(4, correo);
            sentencia.setString(5, notas);

            sentencia.executeUpdate();
        } catch (SQLException sqle){
            sqle.printStackTrace();
        }
    }

    /**
     *
     * @param idCliente
     * metodo el cual permite a nuestro departamento de ventas eliminar cualquier cliente que deje de realizar servicios con nosotros
     */
    public void eliminarCliente(int idCliente){
        String consulta = "DELETE FROM cliente WHERE idCLiente = ?";

        PreparedStatement sentencia = null;
        try {
            sentencia = conexion.prepareStatement(consulta);
            sentencia.setInt(1, idCliente);
            sentencia.executeUpdate();
        } catch (SQLException e){
            e.printStackTrace();
        }
    }

    /**
     *
     * @param nombre
     * @param direccion
     * @param telefono
     * @param correo
     * @param notas
     *
     * metodo el cual permite a nuestro departamento de ventas realizar cualquier modificacion en la ficha tecnica de
     * los clientes
     */
    public void modificarCliente(String nombre, String direccion, int telefono, String correo, String notas
    , int idCliente){
        String consulta = "UPDATE cliente SET nombreCliente = ?, direccionCliente = ?, telefonoCliente = ?, correoCliente = ?," +
                " notasCliente = ? WHERE idCliente = ?";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(consulta);
            sentencia.setString(1, nombre);
            sentencia.setString(2, direccion);
            sentencia.setInt(3, telefono);
            sentencia.setString(4, correo);
            sentencia.setString(5, notas);
            sentencia.setInt(6, idCliente);

            sentencia.executeUpdate();
        } catch (SQLException e){
            e.printStackTrace();
        }
    }

   //funcionalidad botones registro de usuarios

    /**
     *
     * @param nombre
     * @param password
     * @param tipoUsuario
     * metodo el cual permite a nuestro departamento de administracion dar de alta cualquier usuario
     */
    public void altaUsuario(String nombre, String password, String tipoUsuario) {
        String consulta = "INSERT INTO usuario (nombreUsuario, contrasena, tipoUsuario) " +
                "values(?,?,?)";
        PreparedStatement sentencia = null;
        try {
            sentencia = conexion.prepareStatement(consulta);

            sentencia.setString(1, nombre);
            sentencia.setString(2, password);
            sentencia.setString(3, String.valueOf(tipoUsuario));

            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        }
    }

    /**
     *
     * @param idUsuario
     *
     * metodo el cual permite a nuestros departamentos eliminar cualquier usuario de la base de datos
     */
    public void eliminarUsuario(int idUsuario){
        String consulta = "DELETE FROM usuario WHERE idUsuario = ?";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(consulta);
            sentencia.setInt(1, idUsuario);
            sentencia.executeUpdate();
        } catch (SQLException e){
            e.printStackTrace();
        }
    }

    /**
     *
     * @param nombre
     * @param contrasena
     * @param idUsuario
     *
     * metodo el cual permite a nuestros departamentos modificar cualquier usuario de la base de datos
     */
    public void modificarUsuario(String nombre, String contrasena, String tipoUsuario, int idUsuario){
        String consulta = "UPDATE usuario SET nombreUsuario = ?, contrasena = ?, tipoUsuario = ?" +
                " where idUsuario = ?";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(consulta);
            sentencia.setString(1, nombre);
            sentencia.setString(2, contrasena);
            sentencia.setString(3, tipoUsuario);
            sentencia.setInt(4, idUsuario);
            sentencia.executeUpdate();
        } catch (SQLException e){
            e.printStackTrace();
        }
    }

    //metodos resultset

    public ResultSet consultarBusquedaVenta(String buscarProducto) throws SQLException{
        if (conexion == null)
            return null;

        if (conexion.isClosed())
            return null;

        String consulta = "SELECT producto, count(*) FROM pedidoventa WHERE producto = ?";
        PreparedStatement sentencia = null;

        sentencia = conexion.prepareStatement(consulta);
        sentencia.setString(1, String.valueOf(buscarProducto));
        ResultSet resultado = sentencia.executeQuery();

        return resultado;
    }

    public ResultSet consultarBusquedaCompra(String buscarProducto) throws SQLException{
        if (conexion == null)
            return null;

        if (conexion.isClosed())
            return null;

        String consulta = "SELECT producto, count(*) FROM pedidocompra WHERE producto = ?";
        PreparedStatement sentencia = null;

        sentencia = conexion.prepareStatement(consulta);
        sentencia.setString(1, String.valueOf(buscarProducto));
        ResultSet resultado = sentencia.executeQuery();

        return resultado;
    }

    public ResultSet consultarCompra() throws SQLException{
       String sentenciaSQL = "SELECT * FROM pedidocompra";
        PreparedStatement sentencia;
        ResultSet resultSet;

        sentencia = conexion.prepareStatement(sentenciaSQL);
        resultSet = sentencia.executeQuery();
        return resultSet;
    }

    public ResultSet consultarVenta() throws SQLException{
        String sentenciaSQL = "SELECT * FROM pedidoventa";
        PreparedStatement sentencia;
        ResultSet resultSet;

        sentencia = conexion.prepareStatement(sentenciaSQL);
        resultSet = sentencia.executeQuery();
        return resultSet;
    }

    /**
     *
     * @return
     * @throws SQLException
     *
     * metodo que sirve para listar la informacion de la base de datos perteneciente a los clientes
     */
    public ResultSet consultarCliente() throws SQLException{
        String sentenciaSQL = "SELECT idCliente, nombreCliente, direccionCliente, telefonoCliente, correoCliente" +
                ", notasCliente FROM cliente";
        PreparedStatement sentencia;
        ResultSet resultSet;

        sentencia = conexion.prepareStatement(sentenciaSQL);
        resultSet = sentencia.executeQuery();
        return resultSet;
    }

    /**
     *
     * @return
     * @throws SQLException
     *
     * metodo que sirve para listar la informacion de la base de datos perteneciente a los proveedores
     */
    public ResultSet consultarProveedor() throws SQLException{
        String sentenciaSql = "SELECT * FROM proveedor";
        PreparedStatement sentencia;
        ResultSet resultSet;

        sentencia = conexion.prepareStatement(sentenciaSql);
        resultSet = sentencia.executeQuery();
        return resultSet;
    }

    /**
     *
     * @return
     * @throws SQLException
     *
     * metodo que sirve para listar la informacion de la base de datos perteneciente a los usuarios registrados
     */
    public ResultSet consultarRegistroAdministrador() throws SQLException{
        String sentenciaSql = "SELECT * FROM usuario";
        PreparedStatement sentencia;
        ResultSet resultSet;

        sentencia = conexion.prepareStatement(sentenciaSql);
        resultSet = sentencia.executeQuery();
        return resultSet;
    }

    /**
     *
     * @return
     * @throws SQLException
     *
     * metodo que sirve para listar la informacion de la base de datos perteneciente a los usuarios registrados
     */
    public ResultSet consultarRegistroCliente() throws SQLException{
        String sentenciaSql = "SELECT * FROM usuario where tipoUsuario = 'CLIENTE'";
        PreparedStatement sentencia;
        ResultSet resultSet;

        sentencia = conexion.prepareStatement(sentenciaSql);
        resultSet = sentencia.executeQuery();
        return resultSet;
    }

    /**
     *
     * @return
     * @throws SQLException
     *
     * metodo que sirve para listar la informacion de la base de datos perteneciente a los usuarios registrados
     */
    public ResultSet consultarRegistroProveedor() throws SQLException{
        String sentenciaSql = "SELECT * FROM usuario where tipoUsuario = 'PROVEEDOR'";
        PreparedStatement sentencia;
        ResultSet resultSet;

        sentencia = conexion.prepareStatement(sentenciaSql);
        resultSet = sentencia.executeQuery();
        return resultSet;
    }

    /**
     *
     * @param nombre
     * @param password
     * @return
     *
     * metodo que sirve para comprobar si los datos introducidos a la hora de iniciar sesion coinciden con los
     * guardados en la base de datos
     */
    public boolean comprobarDatosUsuario(String nombre, String password, String tipoUsuario) {
        System.out.println("funciona");
        PreparedStatement sentencia = null;
        try {
            String consulta = "SELECT * FROM usuario where nombreUsuario = ? and contrasena = ? and tipoUsuario = ?";
            sentencia = conexion.prepareStatement(consulta);
            sentencia.setString(1, nombre);
            sentencia.setString(2, password);
            sentencia.setString(3, tipoUsuario);

            ResultSet resultado = sentencia.executeQuery();
            if(resultado.next()) {
                return true;
            }
        } catch (SQLException a) {
            a.printStackTrace();
        }
        return false;
    }
}
