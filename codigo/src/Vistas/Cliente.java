package Vistas;

import Enumeradas.TipoUsuario;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;

public class Cliente extends JFrame{
    private final static String TITULOFRAME = "PIEZAS BOMBAZO";
    public JPanel panel1;
    public JTabbedPane tabbedPane1;
    public JButton altaClienteButton;
    public JButton modificarClienteButton;
    public JTable tablaClientes;
    public JButton eliminarClienteButton;
    public JTextArea notasCliente;
    public JTextField correoCliente;
    public JTextField telefonoCliente;
    public JTextField direccionCliente;
    public JTextField nombreCliente;

    //registro
    public JButton eliminarUsuarioButton;
    public JButton modificarUsuarioButton;
    public JTextField usuarioRegistroCliente;
    public JTextField contrasenaRegistroCliente;
    public JComboBox tipoUsuarioCombo;
    public JTable tablaUsuariosCliente;

    //correo
    public JTextField clienteDestinatario;
    public JTextField asuntoCorreo;
    public JTextArea cuerpoCorreo;
    public JButton enviarButton;


    public DefaultTableModel dtmCliente;
    public DefaultTableModel dtmRegistroCliente;

    public Cliente(){
        super(TITULOFRAME);
        initFrame();
    }

    /**
     * Configura la ventana y la hace visible
     */
    private void initFrame() {
        this.setIconImage(new ImageIcon(getClass().getResource("/Image/piezasBombazo.png")).getImage());
        this.setContentPane(panel1);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.pack();
        this.setVisible(false);
        this.setSize(new Dimension(this.getWidth()+200, this.getHeight()+100));
        this.setLocationRelativeTo(null);
        setTableModels();
        setEnumComboBox();
    }

    private void setTableModels(){
        this.dtmCliente = new DefaultTableModel();
        this.tablaClientes.setModel(dtmCliente);

        this.dtmRegistroCliente = new DefaultTableModel();
        this.tablaUsuariosCliente.setModel(dtmRegistroCliente);
    }

    private void setEnumComboBox() {
        for(TipoUsuario constant : TipoUsuario.values()) { tipoUsuarioCombo.addItem(constant.getValor()); }
        tipoUsuarioCombo.setSelectedIndex(-1);

    }

}
