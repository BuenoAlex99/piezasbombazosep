package Vistas;

import Enumeradas.TipoUsuario;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;

public class Proveedor extends JFrame{
    private final static String TITULOFRAME = "PIEZAS BOMBAZO";
    public JPanel panel1;
    public JTabbedPane tabbedPane1;
    
    //proveedor
    public JTextField nombreProveedor;
    public JTextField direccionProveedor;
    public JTextField telefonoProveedor;
    public JTextField correoProveedor;
    public JTextArea notasProveedor;
    public JButton modificarProveedorButton;
    public JButton altaProveedorButton;
    public JTable tablaProveedor;
    
    //registro proveedor
    public JButton eliminarUsuarioButton;
    public JButton modificarUsuarioButton;
    public JTextField UsuarioRegistroProveedor;
    public JTextField ContrasenaRegistroProveedor;
    public JComboBox TipoUsuarioRegistroProveedor;
    public JTable tablaUsuariosRegistro;
    public JButton eliminarProveedorButton;

    public JComboBox tipoUsuarioCombo;

    //enviar correo
    public JTextField proveedorDestinatario;
    public JTextArea cuerpoCorreo;
    public JTextField asuntoCorreo;
    public JButton enviarButton;



    public DefaultTableModel dtmProveedor;
    public DefaultTableModel dtmRegistroProveedor;

    public Proveedor(){
        super(TITULOFRAME);
        initFrame();
    }

    /**
     * Configura la ventana y la hace visible
     */
    private void initFrame() {
        this.setIconImage(new ImageIcon(getClass().getResource("/Image/piezasBombazo.png")).getImage());
        this.setContentPane(panel1);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.pack();
        this.setVisible(false);
        this.setSize(new Dimension(this.getWidth()+200, this.getHeight()+100));
        this.setLocationRelativeTo(null);
        setTableModels();
        setEnumComboBox();
    }

    private void setTableModels(){
        this.dtmProveedor = new DefaultTableModel();
        this.tablaProveedor.setModel(dtmProveedor);

        this.dtmRegistroProveedor = new DefaultTableModel();
        this.tablaUsuariosRegistro.setModel(dtmRegistroProveedor);
    }

    private void setEnumComboBox() {
        for(TipoUsuario constant : TipoUsuario.values()) { tipoUsuarioCombo.addItem(constant.getValor()); }
        tipoUsuarioCombo.setSelectedIndex(-1);

    }
}
