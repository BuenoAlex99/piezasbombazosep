package Vistas;

import Enumeradas.*;
import com.github.lgooddatepicker.components.DatePicker;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;

public class Administrador extends JFrame{
    private final static String TITULOFRAME = "PIEZAS BOMBAZO";
    public JPanel panel1;
    public JTabbedPane tabbedPane1;

    //cliente
    public JTextField nombreClienteTxt;
    public JTextField direccionClienteTxt;
    public JTextField telefonoClienteTxt;
    public JTextField correoClienteTxt;
    public JTable tablaClientes;
    public JButton altaCliente;
    public JButton modificarCliente;
    public JButton eliminarCliente;
    public JTextArea notasClienteTxt;

    //proveedor
    public JTextField nombreProveedorTxt;
    public JTextField direccionProveedorTxt;
    public JTextField telefonoProveedorTxt;
    public JTextField correoProveedorTxt;
    public JTextArea notasProveedorTxt;
    public JButton altaProveedor;
    public JButton modificarProveedor;
    public JButton eliminarProveedor;
    public JTable tablaProveedor;

    //correo
    public JTextField destinatarioTxt;
    public JTextField asuntoCorreo;
    public JTextArea cuerpoCorreo;
    public JButton enviarButton;

    //registro
    public JTextField usuarioAdministracionTxt;
    public JTextField passwordUsuarioAdministracion;
    public JComboBox tipoUsuarioCombo;
    public JTable tablaUsuariosAdministracion;
    public JButton altaUsuario;
    public JButton modificarUsuario;
    public JButton eliminarUsuario;
    public JButton buscarUsuarioButton;
    public JTable tablaBusquedaUsuario;
    public JComboBox buscarTipoUsuario;

    public JButton INICIO;

    //COMPRA
    public JComboBox productoCompraCombo;
    public JTextField cantidadCompra;
    public JTextField precioCompra;
    public JComboBox proveedorCombo;
    public JComboBox indicadorRecibidoCombo;
    public JComboBox indicadorCobroCombo;
    public DatePicker fechaPedidoDatePicker;
    public DatePicker fechaRecibidoDatePicker;
    public JButton ELIMINARcompra;
    public JButton ALTAcompra;
    public JTable tablaCompras;
    public JButton BUSCARcompra;
    public JTable tablaBusquedaCompra;
    public JButton PDFcompra;
    public JComboBox busquedaCompraCombo;

    //VENTA
    public JComboBox productoVentaCombo;
    public JTextField cantidadVenta;
    public JTextField precioVenta;
    public JComboBox clienteCombo;
    public JComboBox indicadorEnvioCombo;
    public JComboBox indicadorCobroVenta;
    public DatePicker fechaPedidoVentaDatePicker;
    public DatePicker fechaEnvioVentaDatePicker;
    public DatePicker fechaCobroVentaDatePicker;
    public JButton ELIMINARventa;
    public JButton ALTAventa;
    public JTable tablaVenta;
    public JButton BUSCARventa;
    public JTable tablaBusquedaVenta;
    public JButton PDFventa;
    public JComboBox busquedaVentaCombo;




    //dtm
    public DefaultTableModel dtmTablaClientes;

    public DefaultTableModel dtmTablaProveedor;

    public DefaultTableModel dtmTablaCompras;
    public DefaultTableModel dtmTablaBusquedaCompras;
    public DefaultTableModel dtmTablaVentas;
    public DefaultTableModel dtmTablaBusquedaVentas;
    public DefaultTableModel dtmTablaUsuarios;
    public DefaultTableModel dtmTablaBusquedaUsuarios;


    public Administrador(){
        super(TITULOFRAME);
        initFrame();
    }

    /**
     * Configura la ventana y la hace visible
     */
    private void initFrame() {
        this.setIconImage(new ImageIcon(getClass().getResource("/Image/piezasBombazo.png")).getImage());
        this.setContentPane(panel1);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.pack();
        this.setVisible(false);
        this.setSize(new Dimension(this.getWidth()+200, this.getHeight()+100));
        this.setLocationRelativeTo(null);
        this.setEnumComboBox();
        setTableModels();
    }

    private void setTableModels(){
        this.dtmTablaClientes = new DefaultTableModel();
        this.tablaClientes.setModel(dtmTablaClientes);

        this.dtmTablaProveedor = new DefaultTableModel();
        this.tablaProveedor.setModel(dtmTablaProveedor);

        this.dtmTablaUsuarios = new DefaultTableModel();
        this.tablaUsuariosAdministracion.setModel(dtmTablaUsuarios);

        this.dtmTablaBusquedaUsuarios = new DefaultTableModel();
        this.tablaBusquedaUsuario.setModel(dtmTablaBusquedaUsuarios);

        this.dtmTablaCompras = new DefaultTableModel();
        this.tablaCompras.setModel(dtmTablaCompras);

        this.dtmTablaBusquedaCompras = new DefaultTableModel();
        this.tablaBusquedaCompra.setModel(dtmTablaBusquedaCompras);

        this.dtmTablaVentas = new DefaultTableModel();
        this.tablaVenta.setModel(dtmTablaVentas);

        this.dtmTablaBusquedaVentas = new DefaultTableModel();
        this.tablaBusquedaVenta.setModel(dtmTablaBusquedaVentas);
    }

    private void setEnumComboBox() {
        for(TipoUsuario constant : TipoUsuario.values()) { tipoUsuarioCombo.addItem(constant.getValor()); }
        tipoUsuarioCombo.setSelectedIndex(-1);

        for (Productos constant : Productos.values()){ productoCompraCombo.addItem(constant.getValor());}
        productoCompraCombo.setSelectedIndex(-1);

        for (Productos constant : Productos.values()){ productoVentaCombo.addItem(constant.getValor());}
        productoVentaCombo.setSelectedIndex(-1);

        for (Productos constant : Productos.values()){ busquedaCompraCombo.addItem(constant.getValor());}
        busquedaCompraCombo.setSelectedIndex(-1);

        for (Productos constant : Productos.values()){ busquedaVentaCombo.addItem(constant.getValor());}
        busquedaVentaCombo.setSelectedIndex(-1);

        for (IndicadorCobro constant : IndicadorCobro.values()){ indicadorCobroVenta.addItem(constant.getValor());}
        indicadorCobroVenta.setSelectedIndex(-1);

        for (IndicadorCobro constant : IndicadorCobro.values()){ indicadorCobroCombo.addItem(constant.getValor());}
        indicadorCobroCombo.setSelectedIndex(-1);

        for (IndicadorRecibido constant : IndicadorRecibido.values()){ indicadorRecibidoCombo.addItem(constant.getValor());}
        indicadorRecibidoCombo.setSelectedIndex(-1);

        for (IndicadorEnvio constant : IndicadorEnvio.values()){ indicadorEnvioCombo.addItem(constant.getValor());}
        indicadorEnvioCombo.setSelectedIndex(-1);
    }
}
