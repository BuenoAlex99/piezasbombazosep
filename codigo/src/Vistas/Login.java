package Vistas;

import Enumeradas.TipoUsuario;

import javax.swing.*;
import java.awt.*;

public class Login extends JFrame{
    private final static String TITULOFRAME = "PIEZAS BOMBAZO";
    public JPanel panel1;
    public JTextField usuarioLogin;
    public JPasswordField contrasenaLogin;
    public JButton ACCEDERButton;
    public JComboBox tipoUsuarioCombo;

    public Login() {
        super(TITULOFRAME);
        initFrame();
    }

    /**
     * Configura la ventana y la hace visible
     */
    private void initFrame() {
        this.setIconImage(new ImageIcon(getClass().getResource("/Image/piezasBombazo.png")).getImage());
        this.setContentPane(panel1);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.pack();
        this.setVisible(true);
        this.setSize(new Dimension(this.getWidth()+200, this.getHeight()+100));
        this.setLocationRelativeTo(null);
        this.setEnumComboBox();
    }

    private void setEnumComboBox() {
        for (TipoUsuario constant : TipoUsuario.values()) {
            tipoUsuarioCombo.addItem(constant.getValor());
        }
        tipoUsuarioCombo.setSelectedIndex(-1);
    }
}
