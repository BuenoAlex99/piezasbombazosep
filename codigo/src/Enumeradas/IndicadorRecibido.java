package Enumeradas;

public enum IndicadorRecibido {
    RECIBIDO("Pedido recibido"),
    NO_RECIBIDO("Pedido no recibido");

    private String valor;

    IndicadorRecibido(String valor){ this.valor = valor; }

    public String getValor() {
        return valor;
    }
}
