package Enumeradas;

public enum IndicadorCobro {
    COBRADO("Pedido pagado"),
    NO_COBRADO("Pedido no pagado");

    private String valor;

    IndicadorCobro(String valor){ this.valor = valor; }

    public String getValor() {
        return valor;
    }
}
