package Enumeradas;

public enum IndicadorEnvio {
    ENVIADO("Pedido enviado"),
    NO_ENVIADO("Pedido no enviado");

    private String valor;

    IndicadorEnvio(String valor){ this.valor = valor; }

    public String getValor() {
        return valor;
    }
}
