package Enumeradas;

public enum TipoUsuario {
    ADMINISTRADOR("ADMINISTRADOR"),
    PROVEEDOR("PROVEEDOR"),
    CLIENTE("CLIENTE");

    private String valor;

    TipoUsuario(String valor){ this.valor = valor; }

    public String getValor() {
        return valor;
    }

}
