package Main;

import MVC.Controlador;
import MVC.Modelo;
import Soporte.Correo;
import Vistas.Administrador;
import Vistas.Cliente;
import Vistas.Login;
import Vistas.Proveedor;

import java.sql.SQLException;

public class Principal {
    public static void main(String[] args) throws SQLException {
        Modelo modelo = new Modelo();
        Administrador administrador = new Administrador();
        Cliente cliente = new Cliente();
        Proveedor proveedor = new Proveedor();
        Login login = new Login();
        Correo correo = new Correo();
        Controlador controlador = new Controlador(modelo, administrador, cliente, proveedor, login, correo);



    }

}
